$(document).ready(function(){
    $( "#term" ).autocomplete({
        source: function(request, response) {
        $.ajax({
            url: 'php/Logic/autocomplete.php',
            dataType: "json",
            data: {
            	term : request.term,
                x : $('input[type=radio][name=x]:checked').val()
            },
            success: function(data) {
                response(data);
            }
        });

    },

    select: function(event, ui)
    {
        $("#term").val(ui.item.value);
        $("#search").submit();
    },

    min_length: 3,
    delay: 300
    });
});