<?php
  include 'php/Model/ClassTerm.php';
  include 'php/Database/datalayer.php';
  include 'php/Logic/translate.php';
?>
<!DOCTYPE html>
<html lang="sr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Пиротски</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom -->
    <link href="css/moda.css" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" href="favicon.png">
    
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-54502946-2', 'auto');
      ga('send', 'pageview');
    </script>

    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.php">Пиротски</a>

          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="active"><a href="index.php">Претрага</a></li>
            <li><a href="dodaj.php">Додај</a></li>
            <li><a href="osajtu.php">О сајту</a></li>
            <li><a href="kontakt.php">Контакт</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container">
      <div class="row biglogo">
        <div class="col-sm-12">
          <a href="index.php"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row search">
        <div class="col-sm-12">
          <form name="search" id="search">
            <input type="radio" name="x" value="1" id="radio1" checked="checked"> 
            <label for="radio1">са пиротског</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="radio" name="x" value="2" id="radio2"> 
            <label for="radio2">на пиротски</label>
            <br>
            <input id="term" type="text" name="term" placeholder="Појам за претрагу">
            <br>
            <button class="btn btn-primary submit">ПРЕТРАГА</button> 
          </form>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row results">
        <div class="col-sm-6 col-sm-push-3">
          <?php
            if(isset($_GET['x']) && isset($_GET['term']))
            {
              if(intval($_GET['x']) == 1)
              {
                $result = find_meaning( cirilica(strtolower($_GET['term'])) );

                if(!empty($result->get_term()))
                {
                  print '<h1>'. $result->get_term() . '</h1>' . '<h4>'. $result->get_meaning() . '</h4>';

                  if($result->get_name() != "")
                  {
                    print '<hr>Додао: ' . $result->get_name();

                    if($result->get_link() != "")
                    print '<a target="_blank" href="' . $result->get_link() . '"> (ФБ ЛИНК)</a>';
                  }
                }
                else
                    print 'Није пронађено значење';
              }
              else if(intval($_GET['x'] == 2))
              {
                if(strlen($_GET['term']) > 0)
                {
                    $result = find_term( cirilica(strtolower($_GET['term'])) );

                    if(!empty($result->get_term()))
                    {
                      print '<h1>'. $result->get_term() . '</h1>' . '<h4>'. $result->get_meaning() . '</h4>';

                      if($result->get_name() != "")
                      {
                        print '<hr>Додао: ' . $result->get_name();

                        if($result->get_link() != "")
                        print '<a target="_blank" href="' . $result->get_link() . '"> (ФБ ЛИНК)</a>';
                      }
                    }
                    else
                        print 'Није пронађено значење';
                }
                else
                    print 'Није пронађено значење';
              }
            }
            else
            {
              $terms = get_random_terms(20);

              if(!empty($terms))
              {
                print '<br><br>Случајни појмови:<br><br>';
                foreach ($terms as $key => $value) 
                {
                  print '<a href="index.php?x=1&term=' . $value->get_term() . '">' . $value->get_term() . ' </a>';
                }
              }
            }
          ?>
        </div>

        <div class="col-md-12" text-center">
          <hr>
          <p>Помозите одржавање сајта донацијама преко <strong>биткоин</strong> адресе: <br>155yTbAZY4LaGUrr1goZ4UnkSifjQtMAHf</p>
        </div>
      </div>
    </div>

    <!-- Jquery libraries -->
    <script src="//code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    
    <!-- Bootstrap js -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom js -->
    <script src="js/script.js"></script>
  </body>
</html>