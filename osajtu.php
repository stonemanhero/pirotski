<!DOCTYPE html>
<html lang="sr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Пиротски</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom -->
    <link href="css/moda.css" rel="stylesheet">

    <!-- Favicon -->
    <link rel="icon" href="favicon.png">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-54502946-2', 'auto');
  ga('send', 'pageview');

</script>
    <nav class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="index.php">Пиротски</a>

          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="index.php">Претрага</a></li>
            <li><a href="dodaj.php">Додај</a></li>
            <li class="active"><a href="osajtu.php">О сајту</a></li>
            <li><a href="kontakt.php">Контакт</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <div class="container">
      <div class="row biglogo">
        <div class="col-sm-12">
          <a href="index.php"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row search">
        <div class="col-sm-6 col-sm-offset-3">
          <p>
          	Циљ сајта је да прикупи старе, али и модерне речи и изразе који се користе у свакодневном говору у јужним и југо-источним деловима Србије као и да омогући превод са српског и на српски језик за појмове који се користе у осталим деловима земље.

              <br><br>
              Метод у коме један човек прикупља речи и изразе је по нашем мишљењу одавно превазиђен. Помоћу овог сајта свако може да учествује у креирању речника који се сваког дана мења, али и задржава стари сјај појмова који су се некада користили. Свесни смо и да је наш метод прикупљања подложнији грешкама, али са друге стране откривање и исправљање грешака је неупоредиво брже. Поносни смо на чињеницу да ћемо увек изаћи у сусрет свакоме ко жели да преузме део или комплетну базу података како би је даље унапредио или искористио као помоћ при писању литературе.

              <br><br>
              Као основа за почетну базу појмова је коришћена <a target="_blank" href="https://www.scribd.com/doc/30300666/Recnik-pirotskog-govora">књига</a> и овом приликом се захваљујемо аутору.

              <br><br>
              Овај сајт је непрофитабилан и никада није и неће наплаћивати услуге које пружа, али са друге стране одржавање сајта кошта, па ја свака <a target="_blank" href="https://www.paypal.me/mkamenovic">ДОНАЦИЈА</a> преко PayPal-а добродошла.
              <br><br>Адреса за донације у <strong>биткоинима</strong>: <br>155yTbAZY4LaGUrr1goZ4UnkSifjQtMAHf
          </p> 
        </div>
      </div>
    </div>

    <br>
    <br>
    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>