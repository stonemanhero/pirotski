<?php
	session_start();

	//	Is user already logged?
	if(isset($_SESSION['auth']))
		if($_SESSION['auth'] == "ok")
			header("Location: index.php");

	// Includes
	include '../Model/ClassUser.php';
	include '../datalayer.php';

	// 1 - register, 0 - fail
	$status = 1;

	// Check name's length, allowed characters
	function check_name($string)
	{
		return strlen($string) >= 2 && preg_match("#^[a-zA-Z0-9_]+$#", $string);
	}

	// Check passwords length and equality
	function check_passwords($pass, $pass2)
	{
		return strlen($pass) > 0 && strlen($pass2) > 0 && $pass === $pass2;
	}

	//	Check if all parameters are received
	if(!isset($_POST['name'], $_POST['email'], $_POST['pass'], $_POST['pass2'], $_POST['code']))
	{
		$_SESSION['info'] = "Please input data in all fields.";
		header("Location: ../../registration.php");
	}

	if(!check_name($_POST['name']))
	{
		$status = 0;
		$_SESSION['info'] = "Name must contain at least two characters.";
	}

	if(!unique_name($_POST['name']))
	{
		$status = 0;
		$_SESSION['info'] = "User with provided name already exists";
	}

	if(!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
	{
		$status = 0;
		$_SESSION['info'] = "Wrong email format.";
	}

	if(!unique_email($_POST['email']))
	{
		$status = 0;
		$_SESSION['info'] = "User with provided email already exists";
	}

	if(!check_passwords($_POST['pass'], $_POST['pass2']))
	{
		$status = 0;
		$_SESSION['info'] = "Password fields are empty or not equal.";
	}

	if(!check_invite_code($_POST['code']))
	{
		$status = 0;
		$_SESSION['info'] = "Wrong invite code.";
	}

	//	Register user
	if($status == 1)
	{
		$user = new User();
		$user->set_name($_POST['name']);
		$user->set_email($_POST['email']);
		$user->set_password($_POST['pass']);

		if(register_user($user))
		{
			$_SESSION['info'] = "Registered! You can log in now.";
			header("Location: ../../login.php");
		}
		else
		{
			$_SESSION['info'] = "Registration failed. Database problem!";
			header("Location: ../../registration.php");
		}
	}
	else
	{
		header("Location: ../../registration.php");
	}
?>