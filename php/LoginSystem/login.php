<?php
	session_start();

	if(isset($_SESSION['auth']))
		if($_SESSION['auth'] == "ok")
			header("Location: ../y35-y-3y-y-yyh3y5/index.php");

	include '../Database/datalayer.php';

	$status = 1;

	if(!isset($_POST['user'], $_POST['pass']))
		$status = 0;

	$pass = login_user($_POST['user']);

	if(empty($pass))
		$status = 0;

	if(!password_verify($_POST['pass'], $pass))
		$status = 0;

	if($status == 1)
		$_SESSION['auth'] = "ok";
	else
		$_SESSION['info'] = "Неисправни подаци";

	header("Location: ../../y35-y-3y-y-yyh3y5/login.php");
?>