<?php
	include '../Database/datalayer.php';
	include 'translate.php';

	$result = array();

	if(intval($_GET['x']) == 1)
	{
		$result = autocomplete_term(cirilica(strtolower($_GET['term'])));
		if(!empty($result))
		{
			foreach ($result as $key => $value)
				$terms[] = $value['term'];

			echo json_encode($terms);
		}
	}
	else
	{
		$result = autocomplete_meaning(cirilica(strtolower($_GET['term'])));
		if(!empty($result))
		{
			foreach ($result as $key => $value)
				$terms[] = $value['meaning'];

			echo json_encode($terms);
		}
	}

	
