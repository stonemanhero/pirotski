<?php
	function cirilica($tekst)
	{
		$tekst = str_replace('ž', 'ж',  $tekst);
		$tekst = str_replace('ć', 'ћ',  $tekst);
		$tekst = str_replace('č', 'ч',  $tekst);
		$tekst = str_replace('dž', 'џ',  $tekst);
		$tekst = str_replace('š', 'ш',  $tekst);
		$tekst = str_replace('đ', 'ђ',  $tekst);

		$tekst = str_replace('dz', 'џ', $tekst);
		$tekst = str_replace('sh', 'ш', $tekst);
		$tekst = str_replace('dj', 'ђ', $tekst);
		$tekst = str_replace('lj', 'љ', $tekst);
		$tekst = str_replace('nj', 'њ', $tekst);
		$tekst = str_replace('cj', 'ћ', $tekst);
		$tekst = str_replace('ch', 'ч', $tekst);
		$tekst = str_replace('zh', 'ж', $tekst);

		$tekst = str_replace('a', 'а', $tekst);
		$tekst = str_replace('b', 'б', $tekst);
		$tekst = str_replace('v', 'в', $tekst);
		$tekst = str_replace('g', 'г', $tekst);
		$tekst = str_replace('d', 'д', $tekst);
		$tekst = str_replace('e', 'е', $tekst);
		$tekst = str_replace('z', 'з', $tekst);
		$tekst = str_replace('i', 'и', $tekst);
		$tekst = str_replace('j', 'ј', $tekst);
		$tekst = str_replace('k', 'к', $tekst);
		$tekst = str_replace('l', 'л', $tekst);
		$tekst = str_replace('m', 'м', $tekst);
		$tekst = str_replace('n', 'н', $tekst);
		$tekst = str_replace('o', 'о', $tekst);
		$tekst = str_replace('p', 'п', $tekst);
		$tekst = str_replace('r', 'р', $tekst);
		$tekst = str_replace('s', 'с', $tekst);
		$tekst = str_replace('t', 'т', $tekst);
		$tekst = str_replace('u', 'у', $tekst);
		$tekst = str_replace('f', 'ф', $tekst);
		$tekst = str_replace('h', 'х', $tekst);
		$tekst = str_replace('c', 'ц', $tekst);

		return $tekst;
	}

	function mala_slova($tekst)
	{
		$tekst = str_replace('Ч', 'ч', $tekst);
		$tekst = str_replace('Џ', 'џ', $tekst);
		$tekst = str_replace('Ш', 'ш', $tekst);
		$tekst = str_replace('Ђ', 'ђ', $tekst);
		$tekst = str_replace('Ж', 'ж', $tekst);
		$tekst = str_replace('Љ', 'љ', $tekst);
		$tekst = str_replace('Њ', 'њ', $tekst);

		$tekst = str_replace('А', 'а', $tekst);
		$tekst = str_replace('Б', 'б',  $tekst);
		$tekst = str_replace('В', 'в',  $tekst);
		$tekst = str_replace('Г', 'г',  $tekst);
		$tekst = str_replace('Д', 'д',  $tekst);
		$tekst = str_replace('Е', 'е',  $tekst);
		$tekst = str_replace('З', 'з',  $tekst);
		$tekst = str_replace('И', 'и',  $tekst);
		$tekst = str_replace('Ј', 'ј',  $tekst);
		$tekst = str_replace('К', 'к',  $tekst);
		$tekst = str_replace('Л', 'л',  $tekst);
		$tekst = str_replace('М', 'м',  $tekst);
		$tekst = str_replace('Н', 'н',  $tekst);
		$tekst = str_replace('О', 'о',  $tekst);
		$tekst = str_replace('П', 'п',  $tekst);
		$tekst = str_replace('Р', 'р',  $tekst);
		$tekst = str_replace('С', 'с',  $tekst);
		$tekst = str_replace('Т', 'т',  $tekst);
		$tekst = str_replace('Ћ', 'ћ',  $tekst);
		$tekst = str_replace('У', 'у',  $tekst);
		$tekst = str_replace('Ф', 'ф',  $tekst);
		$tekst = str_replace('Х', 'х',  $tekst);
		$tekst = str_replace('Ц', 'ц',  $tekst);

		$tekst = str_replace('A', 'a', $tekst);
		$tekst = str_replace('B', 'b',  $tekst);
		$tekst = str_replace('V', 'v',  $tekst);
		$tekst = str_replace('G', 'g',  $tekst);
		$tekst = str_replace('D', 'd',  $tekst);
		$tekst = str_replace('E', 'e',  $tekst);
		$tekst = str_replace('Z', 'z',  $tekst);
		$tekst = str_replace('I', 'i',  $tekst);
		$tekst = str_replace('J', 'j',  $tekst);
		$tekst = str_replace('K', 'k',  $tekst);
		$tekst = str_replace('L', 'l',  $tekst);
		$tekst = str_replace('M', 'm',  $tekst);
		$tekst = str_replace('N', 'n',  $tekst);
		$tekst = str_replace('O', 'o',  $tekst);
		$tekst = str_replace('P', 'p',  $tekst);
		$tekst = str_replace('R', 'r',  $tekst);
		$tekst = str_replace('S', 's',  $tekst);
		$tekst = str_replace('T', 't',  $tekst);
		$tekst = str_replace('U', 'u',  $tekst);
		$tekst = str_replace('F', 'f',  $tekst);
		$tekst = str_replace('H', 'h',  $tekst);
		$tekst = str_replace('C', 'c',  $tekst);

		$tekst = str_replace('Ž', 'zh',  $tekst);
		$tekst = str_replace('Ć', 'cj',  $tekst);
		$tekst = str_replace('Č', 'ch',  $tekst);
		$tekst = str_replace('Dž', 'dz',  $tekst);
		$tekst = str_replace('Š', 'sh',  $tekst);
		$tekst = str_replace('Đ', 'dj',  $tekst);

		return $tekst;
	}
?>