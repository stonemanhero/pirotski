<?php
	session_start();

	include '../Model/ClassContact.php';
	include '../Database/datalayer.php';

	if(isset($_POST['name'], $_POST['email'], $_POST['message']))
	{
		if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL))
		{
			if(strlen($_POST['name']) > 0 && strlen($_POST['message']))
			{
				$contact = new Contact();
				$contact->set_name($_POST['name']);
				$contact->set_email($_POST['email']);
				$contact->set_message($_POST['message']);

				add_message($contact);
				$_SESSION['info'] = "Порука је успешно послата";
			}
			else
				$_SESSION['info'] = "Нисте унели све информације";
		}
		else
			$_SESSION['info'] = "Формат Email-а није валидан";
	}
	else
		$_SESSION['info'] = "Нису послати сви параметри";

	header("Location: ../../kontakt.php");
?>