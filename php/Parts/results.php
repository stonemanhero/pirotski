<?php
    print '<div class="basement">';
    if(isset($_GET['x']) && isset($_GET['term']))
    {
        if(intval($_GET['x']) == 1)
        {
            $result = find_meaning( cirilica(strtolower($_GET['term'])) );

            if(!empty($result->get_term()))
                print '<h1>'. $result->get_term() . '</h1>' . '<h3>'. $result->get_meaning() . '</h3>';
            else
                print 'Није пронађено значење';
        }
        else if(intval($_GET['x'] == 2))
        {
            if(strlen($_GET['term']) > 0)
            {
                $result = find_term( cirilica(strtolower($_GET['term'])) );

                if(!empty($result->get_term()))
                    print '<h1>'. $result->get_term() . '</h1>' . '<h3>'. $result->get_meaning() . '</h3>';
                else
                    print 'Није пронађено значење';
            }
            else
                print 'Није пронађено значење';
        }
    }
    print '</div>';
?> 