<?php
	class Contact
	{
		private $id;
		private $name;
		private $email;
		private $message;
		private $status;


		//setters
		public function set_name($new_name)
		{
			$this->name = $new_name;
		}

		public function set_id($new_id)
		{
			$this->id = $new_id;
		}

		public function set_email($new_email)
		{
			$this->email = $new_email;
		}

		public function set_message($new_message)
		{
			$this->message = $new_message;
		}

		public function set_status($new_status)
		{
			$this->status = $new_status;
		}

		//getters
		public function get_id()
		{
			return $this->id;
		}

		public function get_name()
		{
			return $this->name;
		}

		public function get_email()
		{
			return $this->email;
		}

		public function get_message()
		{
			return $this->message;
		}

		public function get_status()
		{
			return $this->status;
		}
	}
?>