<?php
	class Login
	{
		private $id;
		private $username;
		private $password;
		private $token;
		private $status;

		//setters
		public function set_id($new_id)
		{
			$this->id = $new_id;
		}

		public function set_status($new_status)
		{
			$this->status = $new_status;
		}

		public function set_username($new_username)
		{
			$this->username = $new_username;
		}

		public function set_password($new_password)
		{
			$this->password = $new_password;
		}

		public function set_token($new_token)
		{
			$this->token = $new_token;
		}

		//getters
		public function get_id()
		{
			return $this->id;
		}

		public function get_status()
		{
			return $this->status;
		}

		public function get_username()
		{
			return $this->username;
		}

		public function get_password()
		{
			return $this->password;
		}

		public function get_token()
		{
			return $this->token;
		}
	}
?>