<?php
	class Term
	{
		private $id;
		private $term;
		private $meaning;
		private $name;
		private $link;
		private $status;

		function __construct(){
			$this->name = "";
			$this->link = "";
		}

		//setters
		public function set_id($new_id)
		{
			$this->id = $new_id;
		}

		public function set_status($new_status)
		{
			$this->status = $new_status;
		}

		public function set_term($new_term)
		{
			$this->term = $new_term;
		}

		public function set_meaning($new_meaning)
		{
			$this->meaning = $new_meaning;
		}

		public function set_name($new_name)
		{
			$this->name = $new_name;
		}

		public function set_link($new_link)
		{
			$this->link = $new_link;
		}

		//getters
		public function get_id()
		{
			return $this->id;
		}

		public function get_status()
		{
			return $this->status;
		}

		public function get_term()
		{
			return $this->term;
		}

		public function get_meaning()
		{
			return $this->meaning;
		}

		public function get_name()
		{
			return $this->name;
		}

		public function get_link()
		{
			return $this->link;
		}
	}
?>