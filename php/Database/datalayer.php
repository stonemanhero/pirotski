<?php
	include 'db.php';
	
	function find_meaning($term)
	{
		$db = new DataLayer();
		$db->query("SET NAMES UTF8");
		$db->execute();

		$db->query('SELECT term, meaning, name, link FROM terms WHERE status=1 AND term=:term');
		$db->bind('term', $term);
		$db->execute();

		$term = new Term();
		$term->set_term($db->single()['term']);
		$term->set_meaning($db->single()['meaning']);
		$term->set_name($db->single()['name']);
		$term->set_link($db->single()['link']);

		return $term;
	}

	function find_term($meaning)
	{
		$db = new DataLayer();
		$db->query("SET NAMES UTF8");
		$db->execute();

		$db->query('SELECT term, meaning, name, link FROM terms WHERE status=1 AND meaning=:meaning');
		$db->bind('meaning', $meaning);
		$db->execute();
		
		$term = new Term();
		$term->set_term($db->single()['term']);
		$term->set_meaning($db->single()['meaning']);
		$term->set_name($db->single()['name']);
		$term->set_link($db->single()['link']);

		return $term;
	}

	function autocomplete_term($term)
	{
		$term = '%' . $term . '%';
		$db = new DataLayer();
		$db->query("SET NAMES UTF8");
		$db->execute();
		$db->query('SELECT term FROM terms WHERE status=1 AND term LIKE :term ORDER BY LENGTH(term) ASC LIMIT 8');
		$db->bind('term', $term);
		$db->execute();
		return $db->resultset();
	}

	function autocomplete_meaning($term)
	{
		$term = '%' . $term . '%';
		$db = new DataLayer();
		$db->query("SET NAMES UTF8");
		$db->execute();
		$db->query('SELECT meaning FROM terms WHERE status=1 AND meaning LIKE :term ORDER BY LENGTH(meaning) ASC LIMIT 8');
		$db->bind('term', $term);
		$db->execute();
		return $db->resultset();
	}

	function term_exist($term)
	{
		$db = new DataLayer();
		$db->query("SET NAMES UTF8");
		$db->execute();
		$db->query('SELECT id FROM terms WHERE status=1 AND term=:term');
		$db->bind(':term', $term);
		$db->execute();
		$id = $db->single();

		if($id != 0)
			return $id;

		return 0;
	}

	function get_random_terms($limit)
	{
		$db = new DataLayer();
		$db->query("SET NAMES UTF8");
		$db->execute();

		$db->query('SELECT term FROM terms WHERE status=1 ORDER BY RAND() LIMIT :lmt');
		$db->bind(':lmt', $limit);
		$db->execute();

		$result = $db->resultset();
		$terms = array();

		if(!empty($result))
		{
			foreach ($result as $key => $value) 
			{
				$term = new Term();
				$term->set_term($value['term']);
				array_push($terms, $term);
			}

			return $terms;
		}
		else
		{
			return null;
		}
	}

	function add_term($term)
	{
		$db = new DataLayer();
		$db->query("SET NAMES UTF8");
		$db->execute();

		$db->query('INSERT INTO terms (term, meaning, name, link) VALUES (:term, :meaning, :name, :link)');
   		$db->bind(':term', $term->get_term());
   		$db->bind(':meaning', $term->get_meaning());
   		$db->bind(':name', $term->get_name());
   		$db->bind(':link', $term->get_link());
   		$db->execute();

		return true;
	}

	function add_message($contact)
	{
		$db = new DataLayer();
		$db->query('INSERT INTO contact (name, email, message) VALUES (:name, :email, :message)');
   		$db->bind(':name', $contact->get_name());
   		$db->bind(':email', $contact->get_email());
   		$db->bind(':message', $contact->get_message());
   		$db->execute();

		return true;
	}

	function count_new_terms()
	{
		$db = new DataLayer();
		$db->query('SELECT COUNT(*) FROM terms WHERE status=0');
		$db->execute();
		return $db->single()['COUNT(*)'];
	}

	function count_new_contacts()
	{
		$db = new DataLayer();
		$db->query('SELECT COUNT(*) FROM contact WHERE status=0');
		$db->execute();
		return $db->single()['COUNT(*)'];
	}

	function new_terms()
	{
		$db = new DataLayer();
		$db->query("SET NAMES UTF8");
		$db->execute();
		$db->query('SELECT * FROM terms WHERE status=0 ORDER BY id DESC');
		$db->execute();

		$result = $db->resultset();
		$terms = array();

		if(!empty($result))
		{
			foreach ($result as $key => $value) 
			{
				$term = new Term();
				$term->set_id($value['id']);
				$term->set_term($value['term']);
				$term->set_meaning($value['meaning']);
				$term->set_status($value['status']);

				array_push($terms, $term);
			}

			return $terms;
		}
		else
		{
			return null;
		}
	}

	function update_term($term)
	{
		$db = new DataLayer();
		$db->query("SET NAMES UTF8");
		$db->execute();
		$db->query('UPDATE terms SET term=:term, meaning=:meaning, status=:status WHERE id=:id');
		$db->bind(':id', $term->get_id());
		$db->bind(':term', $term->get_term());
		$db->bind(':meaning', $term->get_meaning());
		$db->bind(':status', $term->get_status());
		$db->execute();

		return true;
	}

	function get_unread_contacts()
	{
		$db = new DataLayer();
		$db->query("SET NAMES UTF8");
		$db->execute();
		$db->query('SELECT * FROM contact WHERE status=0 ORDER BY id DESC');
		$db->execute();

		$result = $db->resultset();
		$contacts = array();

		if(!empty($result))
		{
			foreach ($result as $key => $value) 
			{
				$contact = new Contact();
				$contact->set_id($value['id']);
				$contact->set_name($value['name']);
				$contact->set_email($value['email']);
				$contact->set_message($value['message']);
				$contact->set_status($value['status']);

				array_push($contacts, $contact);
			}

			return $contacts;
		}
		else
		{
			return null;
		}
	}

	function update_contact($contact)
	{
		$db = new DataLayer();
		$db->query("SET NAMES UTF8");
		$db->execute();
		$db->query('UPDATE contact SET status=:status WHERE id=:id');
		$db->bind(':id', $contact->get_id());
		$db->bind(':status', $contact->get_status());
		$db->execute();

		return true;
	}


	function login_user($user)
	{
		$db = new DataLayer();
		$db->query('SELECT password FROM user WHERE username=:user AND status<>0');
		$db->bind('user', $user);
		$db->execute();

		return $db->single()['password'];
	}


	//	Fake admin log
	function log_fake_admin($login)
	{
		$db = new DataLayer();
		$db->query('INSERT INTO fake_login (username, password, token) VALUES (:username, :password, :token)');
   		$db->bind(':username', $login->get_username());
   		$db->bind(':password', $login->get_password());
   		$db->bind(':token', $login->get_token());
   		$db->execute();

		return true;
	}
?>