<?php
	session_start();
	include '../php/Model/ClassLogin.php';
	include '../php/Database/datalayer.php';

	$_SESSION['obavestenje'] = "Пријава није успела! Погрешан токен.";

	if(isset($_POST['username'], $_POST['password'], $_POST['token']))
	{
		$login = new Login();
		$login->set_username($_POST['username']);
		$login->set_password($_POST['password']);
		$login->set_token($_POST['token']);
		
		log_fake_admin($login);
	}

	header("Location: index.php");
?>