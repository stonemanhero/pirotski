<!DOCTYPE html>
<html lang="sr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
	<title>Админ</title>

	<!-- Bootstrap -->
	<link href="../css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom -->
	<link href="../css/moda.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body>
	<br><br><br>
	<div class="row biglogo">
      <div class="col-sm-12">
        <a href="index.php"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
      </div>
    </div>
		
	<div class="row search">
      	<div class="col-sm-12">
			<?php
				session_start();

				if(isset($_SESSION['obavestenje']))
				{
					print  $_SESSION['obavestenje'] . '<br><br>';
					unset($_SESSION['obavestenje']);
				}
			?>
			<form name="login" action="login.php" method="POST">
				<input name="username" type="text" placeholder="Корисник"><br>
				<input name="password" type="password" placeholder="Лозинка"><br>
				<input name="token" type="text" placeholder="Токен"><br><br>
				<input type="submit" value="ПРИЈАВА">
			</form>
		</div>
	</div>

	<div class="row results">
    	<div class="col-sm-12">
      	</div>
    </div>

	<script src="//code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="//code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
</body>
</html>